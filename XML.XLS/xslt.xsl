<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<head>
				<h1>
					Title of this web page?
				</h1>
			</head>
			<body>
				<p>
					The table below describes all computer labs of the faculty
				</p>
				<xsl:for-each select="faculty/laboratory">
					<p>
						Some laboratory
					</p>
					<table border="1">
						<tr bgcolor="#9acd32">
							<th>Number</th>
							<th>Vendors</th>
							<th>Size</th>
							<th>CPU</th>
							<th>RAM</th>
							<th>HDD</th>
						</tr>
						<xsl:for-each select="computer">
							<tr>
								<td><xsl:value-of select="number"/></td>
								<td><xsl:value-of select="vendors"/></td>
								<td><xsl:value-of select="size"/></td>
								<td><xsl:value-of select="cpu"/></td>
								<td><xsl:value-of select="ram"/></td>
								<td><xsl:value-of select="hdd"/></td>
							</tr>
					  </xsl:for-each>
					</table>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>