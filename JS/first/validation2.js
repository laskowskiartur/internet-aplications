function validateFirst(){
	var first = document.forms["form"]["firstname"].value;
	if(first.replace(/\s/g,"") == ""){
		document.getElementById("first_allert").innerHTML = "WRONG";
	}
	else {
		document.getElementById("first_allert").innerHTML = "";
	}
}
function validateLast(){
	var last  = document.forms["form"]["lastname"].value;
	if(last.replace(/\s/g,"") == ""){
		document.getElementById("last_allert").innerHTML = "WRONG";
	}
	else {
		document.getElementById("last_allert").innerHTML = "";
	}
}
function validateEmail(){
	var email = document.forms["form"]["emailaddress"].value;
	if(email.search(/^[a-zA-z0-9.]*@[[a-zA-z0-9.]*[a-zA-z0-9]$/) == -1){
		document.getElementById("email_allert").innerHTML = "WRONG";
	}
	else {
		document.getElementById("email_allert").innerHTML = "";
	}
}
function validateZip(){
	var zip = document.forms["form"]["zipcode"].value;
	if(zip.search(/^\d\d-\d\d\d$/) == -1){
		document.getElementById("zip_allert").innerHTML = "WRONG";
	}
	else {
		document.getElementById("zip_allert").innerHTML = "";
	}
}
function validateChild() {
	var child = document.forms["form"]["numberofchildren"].value;
	if(child.search(/^[0-9]*$/) == -1){
		document.getElementById("child_allert").innerHTML = "WRONG";
	}
	else {
		document.getElementById("child_allert").innerHTML = "";
	}
}
function validateSex() {
	var sex = document.getElementById("female");
	if( sex.checked == true ){
		document.getElementById("noc").innerHTML = 'Liczba dzieci: <input type="text" name="numberofchildren" onblur="validateChild()">';
	}
	else {
		document.getElementById("noc").innerHTML = "";
	}
}