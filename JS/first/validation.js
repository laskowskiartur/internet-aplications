function myValidation(){
	var first, last, email, zip, tmp, child;
	// Get the value of firstname
	first = document.forms["form"]["firstname"].value;
	last  = document.forms["form"]["lastname"].value;
	email = document.forms["form"]["emailaddress"].value;
	zip   = document.forms["form"]["zipcode"].value;
	child = document.forms["form"]["numberofchildren"].value;
	
	// not empty
	if(first == null || first == "") {
		alert("Name must by filled out");
	}
	if(last == null ||last  == "") {
		alert("Lastname must by filled out");
	}
	if(email == null ||email  == "") {
		alert("E-mail must by filled out");
	}
	if(zip == null ||zip  == "") {
		alert("Zip must by filled out");
	}
	
	// without whitespaces
	tmp = first.replace(/\s/g,"");
	if(tmp == ""){
		alert("Name must not be white");
	}
	tmp = last.replace(/\s/g,"");
	if(tmp == ""){
		alert("Lastname must not be white");
	}
	tmp = email.replace(/\s/g,"");
	if(tmp == ""){
		alert("E-mail must not be white");
	}
	tmp = zip.replace(/\s/g,"");
	if(tmp == ""){
		alert("Zip must not be white");
	}
	
	// children number only digits
	if(child.search(/^[0-9]*$/) == -1){
		alert("Number of chldren must be positive or empty!");
	}
	// zip must be 5 digits
	if(zip.search(/^\d\d\d\d\d$/) == -1){
		alert("Zip must be five digits");
	}
	// email contain @ and .
	if(email.search(/^[a-zA-z0-9.]*@[[a-zA-z0-9.]*[a-zA-z0-9]$/) == -1){
		alert("Email must be in form 'name@domail.domain'");
	}
}